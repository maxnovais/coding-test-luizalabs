from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.reverse import reverse

from employee_app.models import Employee


class EmployeeTests(APITestCase):
    def create_employee(self, name, email, department=None):
        url = reverse('employee-list')
        data = {'name': name, 'email': email, 'department': department}
        response = self.client.post(url, data, format='json')
        return response

    def employee_dict(self):
        return {'name': 'Luke Skywalker', 'email': 'luke@maytheforce.with.us', 'department': 'Rebels'}

    def test_success_to_create_employee(self):
        employee = self.employee_dict()
        response = self.create_employee(**employee)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 1)

    def test_failed_to_create_employee_with_wrong_email_format(self):
        employee = self.employee_dict()
        employee['email'] = 'lukemaytheforcewithus'
        response = self.create_employee(**employee)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Employee.objects.count(), 0)

    def test_failed_to_create_two_employee_with_same_email(self):
        employee = self.employee_dict()
        response1 = self.create_employee(**employee)
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)

        response2 = self.create_employee(**employee)
        self.assertEqual(response2.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Employee.objects.count(), 1)

    def test_success_to_create_two_employee_with_same_name_only(self):
        employee = self.employee_dict()
        response1 = self.create_employee(**employee)
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)

        employee['email'] = 'other_luke@maytheforce.with.us'
        response2 = self.create_employee(**employee)
        self.assertEqual(response2.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 2)

    def test_success_to_create_a_employee_without_department(self):
        employee = self.employee_dict()
        del employee['department']
        response1 = self.create_employee(**employee)
        self.assertEqual(response1.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), 1)

    def test_success_in_list_all_employees(self):
        employee1 = self.employee_dict()
        self.create_employee(**employee1)
        employee2 = {'name': 'Anakin Skywalker', 'email': 'anakin@maytheforce.with.us', 'department': 'Empire'}
        self.create_employee(**employee2)
        employee_list = [employee1, employee2]
        url = reverse('employee-list')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), employee_list)

    def test_success_in_retrieve_one_employee(self):
        employee1 = self.employee_dict()
        self.create_employee(**employee1)
        employee2 = {'name': 'Anakin Skywalker', 'email': 'anakin@maytheforce.with.us', 'department': 'Empire'}
        self.create_employee(**employee2)
        url = reverse('employee-detail', args=[2])
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json(), employee2)

    def test_success_in_edit_employee_using_patch(self):
        employee = self.employee_dict()
        self.create_employee(**employee)
        url = reverse('employee-detail', args=[1])
        data = {'name': 'Luke S.'}
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['name'], 'Luke S.')

    def test_success_in_edit_employee_using_put(self):
        employee = self.employee_dict()
        self.create_employee(**employee)
        url = reverse('employee-detail', args=[1])
        data = {'name': 'Luke S.', 'email': 'luke@maytheforce.with.us', 'department': 'Rebels'}
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['name'], 'Luke S.')

    def test_success_in_remove_employee_using_delete(self):
        employee = self.employee_dict()
        self.create_employee(**employee)
        url = reverse('employee-detail', args=[1])
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Employee.objects.count(), 0)
