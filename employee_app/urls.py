from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from employee_app.views import EmployeeViewSet
from rest_framework_swagger.views import get_swagger_view


router = DefaultRouter()
router.register(r'employee', EmployeeViewSet)

schema_view = get_swagger_view(title='Employee API')

urlpatterns = [
    url('^$', schema_view),
    url(r'^', include(router.urls)),
]