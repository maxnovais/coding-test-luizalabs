from django.db import models


class Employee(models.Model):
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150, unique=True)
    department = models.CharField(max_length=50, null=True)

    class Meta:
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'

    def __str__(self):
        return '{} ({})'.format(self.name, self.email)
