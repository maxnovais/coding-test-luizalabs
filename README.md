# Django Code Testing - Luiza Labs

Esse é um projeto feito exclusivamente para o teste de codificação do Luiza Labs
utilizando o Python 3.6.1 e o framework Django.

## Índice

* Especificações Técnicas
* Instalação do Projeto
* Usando a API
  * Criando um Novo Funcionário
  * Listando todos os funcionários 
  * Retornando um funcionário 
  * Editando através de PATCH 
  * Editando através de PUT 
  * Excluíndo funcionário 
* Admin
* Testes Automatizados


## 1. Especificações Técnicas

- Python 3.6.1
- Django 1.11
- Django-Rest-Framework
- Swagger
- Nose

**Observação:** Não fiz testes com versões anteriores de Python ou Django.

## 2. Instalação do Projeto

Em um terminal, clone o projeto

```shell
$ git clone git@bitbucket.org:maxnovais/coding-test-luizalabs.git
```

Crie seu ambiente em seu próprio gerenciador de virtualenv (virtualenv, wraper, 
pyenv, etc.), inicialize-o e instale as dependências necessárias:

```shell
$ pip install -r requirements.txt
```

Rode as migrações do banco de dados usando o comando, pela simplicidade, optei
em usar o próprio sqlite3.

```shell
$ python manage.py migrate 
```

Após isso, inicialize o servidor:

```shell
$ python manage.py runserver
```

## 3. Usando a API

O método mais ágil, é realizar o processo direto pelo *Swagger*, acessando em
seu navegador a url: <http://localhost:8000/>

Ou também pode usar curl, como abaixo:

### 3.1. Criando um novo funcionário

```shell
$ curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"name": "luke", "email": "luke@mayforcebe.with.us", "department": "Rebels"}' 'http://localhost:8000/employee/'
{"name":"luke","email":"luke@mayforcebe.with.us","department":"Rebels"}
```

### 3.2. Listando todos os funcionários

```
$ curl -X GET --header 'Accept: application/json' 'http://localhost:8000/employee/'
[{"name":"luke","email":"luke@mayforcebe.with.us","department":"Rebels"}]
```

### 3.3. Retornando um funcionário

```shell
$ curl -X GET --header 'Accept: application/json' 'http://localhost:8000/employee/1/'
{"name":"luke","email":"luke@mayforcebe.with.us","department":"Rebels"}
```

### 3.4. Editando através de PATCH

```shell
$ curl -X PATCH --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "name": "Luke S." }' 'http://localhost:8000/employee/1/' 
{"name":"Luke S.","email":"luke@mayforcebe.with.us","department":"Rebels"}
```

### 3.5. Editando através de PUT

```
$ curl -X PUT --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{"name": "luke", "email": "luke@mayforcebe.with.us", "department": "Rebels"}' 'http://localhost:8000/employee/1/'
{"name":"luke","email":"luke@mayforcebe.with.us","department":"Rebels"}
```

### 3.6. Excluíndo funcionário

```shell
$ curl -X DELETE --header 'Accept: application/json' 'http://localhost:8000/employee/1/'
```

## 4. Admin

No projeto, é usado o admin padrão do Django, então, para criar um usuário é só seguir
o padrão já estabelecido pelo framework:

```shell
$ python manage.py createsuperuser
```

É possível acessar a interface através da url <http://localhost:8000/admin>

## 5. Testes Automatizados

Não há muitos testes automatizados, pois não há referência no enunciado quais regras
do modelo, eu apliquei algumas regras ao modelo para aplicar alguns testes.

```python
class Employee(models.Model):
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150, unique=True)
    department = models.CharField(max_length=50, null=True)
```

Os testes rodam usando o comando:

```shell
$ python manage.py test
```

Esse projeto utiliza o [Pipelines do Bitbucket][1] para rodar os testes via CI.

[1]: https://bitbucket.org/maxnovais/coding-test-luizalabs/addon/pipelines/home#!/